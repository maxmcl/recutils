function [crit, pars] = genCrit(Fant, Sino, DATADIR, MPDIR, pars)
%% GenCrit - Building an object representing the objective function
%   The geometric characteristics of the problem are obtained from the XCAT
%   phantom and the sinogram in the input arguments.
%   Inputs:
%   - Fant: phantom (FantomeXCAT) used to produce the sinogram
%   - Sino: sinogram (Sino_XCAT) produced from the phatnom
%   - DATADIR: folder where the phantom and the sinogram are saved
%   - MPDIR: folder where the projection matrix will be saved
%   - lambda: penalization parameter (both L2 & L2L1 penalty functions)
%   - pars: struct that contains the various parameters required. Should contain
%   the penalty functions parameters (lambda, lambdaImag, delta), the
%   coordinates type, the penalty function type and the imaginary penalty
%   function boolean.
%   Output:
%   - crit : object representing the objective function (class
%   Critere) consisting of two terms: an Adequation object and a
%   Penalization object, respectively representing the least square and the
%   penalization function parts of the objective function.
%   - pars: the updated parameter structure with the default values for the
%   missing parameters.

%% Retrieving parameters from pars structure and setting default values
pars = checkPars(pars);

%% Properties of the projection matrix
GeoObj = 'Circ';
nRayons = 3;
[~, maxThreads] = nCPUs();

%% Creating the projection matrix
Srie = Fant.creationSerie(DATADIR, 'GeoObj', GeoObj);
[~, ~, ~] = mkdir(MPDIR); % Avoiding the warning in case directory exists

% Variations according to coordinates
if strcmp(pars.coordType, 'cart') % Cartesian coordinates are used
    UsedSym = 'quad';
%     warning('double');
    GeoM_C = GeoM_Factory.create(Srie.GeoS, Sino, ...
        'nRayons', nRayons, 'UsedSym', UsedSym);
    MP = MP_Cart(containers.Map({'GeoM', 'MatDir', 'nThreads'}, ...
        {GeoM_C, MPDIR, min(maxThreads, 4)}));
else % Cylindrical coordinates are used
    SparseRep = 'ICCS';
    SparseMode = 'MatVec';
    Thetas = Sino.GeoP.nProjections;
    Rhos = round(Srie.GeoS.Rows * Srie.GeoS.Columns / ...
        Thetas);
    Srie = Srie.cvPol(Rhos, Thetas);

    GeoM_P = GeoM_Factory.create(Srie.GeoS, Sino, ...
        'nRayons', nRayons);
    MP = MP_Pol(containers.Map({'GeoM', 'nThreads', 'SparseRep', ...
        'SparseMode', 'MatDir'}, ...
        {GeoM_P, maxThreads, SparseRep, SparseMode, MPDIR}));
end

%% Building the objective function
% RESTRICTING TO Linear_Gauss_noPond for now
Adeq = Linear_Gauss_noPond(containers.Map('MPrj', MP));
% Building the penalty function chosen by the user
Penal = eval([pars.penalType, ('(containers.Map({''GeoS'', ') ...
    (' ''lambda'', ''delta''}, {Srie.GeoS, pars.lambda, pars.delta}))')]);
% So-called original terms of the reconstruction problem
J = {Adeq, Penal};

% Building the Critere object from the previous terms
crit = Critere(containers.Map('J', J));
end

function pars = checkPars(pars)
%% Initializing the default parameters in case they are missing
% Building the penalty function chosen by the user
if ~isfield(pars, 'delta')
    pars.delta = 5e-3;
end
if isfield(pars, 'penalType')
    if strcmp(pars.penalType, 'PenalObj_L2') == 1 || ...
            strcmp(pars.penalType, 'PenalGradObj_L2') == 1
        if ~isfield(pars, 'lambda')
            pars.lambda = 1e-00;
        end
    elseif strcmp(pars.penalType, 'PenalObj_L2L1') == 1 || ...
            strcmp(pars.penalType, 'PenalGradObj_L2L1') == 1
        if ~isfield(pars, 'lambda')
            pars.lambda =  2 * pars.delta * 1e-00;
        end
    else
        fprintf('\nUnrecognized penalty function type, using PenalObj_L2\n');
        pars.penalType = 'PenalObj_L2';
        pars.lambda = 1e-00;
    end
else
    fprintf('\nUnspecified penalty function type, using PenalObj_L2\n');
    pars.penalType = 'PenalObj_L2';
    pars.lambda = 1e-00;
end
if isfield(pars, 'coordType')
    if strcmp(pars.coordType, 'cart') == 0 && strcmp(pars.coordType, ...
            'cyl') == 0
        fprintf('\nUnrecognized coord type, using cylindrical\n');
        pars.coordType = 'cyl';
    end
else
    fprintf('\nUnspecified coord type, using cylindrical\n');
    pars.coordType = 'cyl';
end
end
