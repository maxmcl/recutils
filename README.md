README
======
A repository that contains files used to generate the phantom and the objective function terms for the reconstruction problem. This repository is required for every /<Method>-reconst/ repository. Make sure that it is correctly placed under your "ROOTDIR" directory. The ```setPaths.m``` file included in this repository adds the other repositories' path to the MATLAB path. It will therefore have to be changed if different paths are used or if repositories are missing.

### What is this repository for? ###

* Testing the minConf solver on tomographic reconstruction problems.

### How do I get set up? ###

To configure this repository, you will need to place it correctly under your "ROOTDIR" directory, where every other repositories are located. It is also likely that you will have to edit ```setPaths.m``` to ensure that every directory is correctly included in MATLAB's path.

### Contribution guidelines ###


### Who do I talk to? ###
