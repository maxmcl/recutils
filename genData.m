function [Fant, Sino] = genData(DATADIR, factor, name)
%% genData - Generate a phantom and the associated sinogram
%   Input:
%   - DATADIR: folder where the phantom and sinograms will be saved
%   - factor: resolution increase or decrease compared to the original
%   parameters (1 = same)
%   - name: name under which the phantom files will be saved in DATADIR
%   Outputs:
%   - Fant: phantom created using XCAT
%   - Sino: sinogram of the phantom

%% Initialization
if factor < 1
    error('Factor must be >= 1');
end

global ROOTDIR
HERE = pwd;
ct_project = 'ct_project_LINUX'; % XCAT parameter, depends on OS

% Patient
PatientsName = name;
PatientsSex = 'Male';
% Properties of the phantom
Bras = 0;
nbPixels = 512 / factor;
taillePixels = 0.7 * factor; % mm
epaisseurTranches = 1; % mm
trancheIni = 1200;
trancheFin = 1200;
% Properties of the tomograph
tomo = 'Siemens2D';
FFS = 'none';
TablePosition = -1199;
RefAngleDeg = 0;
ficsource = 'sp120_5.txt';
nRayonsG = 6;
mAs = 0.01293 / nRayonsG;
Poisson = true;

%% Building the phantom
% Patient & study are required arguments for FantomeXCAT object
Pat = Patient(containers.Map({'PatientsName', 'PatientsSex'}, ...
    {PatientsName, PatientsSex}));
Stdy = Study();

% A phantom is required to create a sinogram!
Fant = FantomeXCAT(containers.Map({'Patient', 'Study', 'Bras', ...
    'taillePixels', 'nbPixels', 'epaisseurTranches', ...
    'trancheIni', 'trancheFin'}, ...
    {Pat, Stdy, Bras, ...
    taillePixels, nbPixels, epaisseurTranches, ...
    trancheIni, trancheFin}));

%% Building the sinogram
% Geometry protocol with nRayonsG rays by detector
% DETERMINES THE SCANNER'S PROPERTIES
GeoP = GeoP_Factory.create(tomo, 'FFS', FFS);
% Here, we are rebuilding another GeoP object because we have to specify
% additional parameters that aren't handled by the generic GeoP_Factory
% method...
% --
[Props, ~] = findPropsWithDef(GeoP);
Vals = cell(size(Props));
for iProp = 1:numel(Props)
    if isa(GeoP.(Props{iProp}), 'matlab.mixin.Copyable')
        Vals{iProp} = copy(GeoP.(Props{iProp}));
    else
        Vals{iProp} = GeoP.(Props{iProp});
    end
end
MapGeoP = containers.Map(Props, Vals);
MapGeoP('nDetectors') = nRayonsG * MapGeoP('nDetectors');
MapGeoP('FanAngleIncrement') = MapGeoP('FanAngleIncrement') / nRayonsG;
% -- The part above could be removed if GeoP_Factory was fixed...
MapSino = containers.Map({'Fant', 'GeoP', ...
    'ParP', ...
    'TablePosition', 'RefAngleDeg'}, ...
    {Fant, GeometrieProtocole(MapGeoP), ...
    ParP_XCAT(containers.Map({'mAs', 'source', 'Poisson'}, ...
    {mAs, struct('Fichier', fullfile(ROOTDIR, 'XCAT', ct_project, ...
    ficsource)), Poisson})), ...
    TablePosition, RefAngleDeg});
% Finally building the sinogram...
% This will call a Sino_Factory method, either:
% Somatom || SkyScan || XCAT
Sino = Sino_Factory.create('XCAT', 'Map', MapSino', ...
    'dir', DATADIR);

%% Removing projections according to the factor used
Sino.fusionDetectors(nRayonsG * factor);
if factor > 1
    Sino.seProjections(factor, 1);
end

end
