function setPaths
%% SetPaths - Function that adds the required directories to the path
% ROOTDIR must be initialized in the function that calls setPaths
global ROOTDIR
% The following dependencies must be located directly under ROOTDIR
% Using genPathNoGit to add all subdirectories of major directories
% Assuming the user is running from a /<Method>-reconst/ directory

% Add main ROOT directory, where +model should be located
addpath(ROOTDIR);

% Other libraries used in the previous reconstruction algorithms
addpath(genPathNoGit(fullfile(ROOTDIR, 'nlplab')));
addpath(fullfile(ROOTDIR, 'Spot'));

% Data generation repositories
addpath(genPathNoGit(fullfile(ROOTDIR, 'Objets')));
addpath(genPathNoGit(fullfile(ROOTDIR, 'XCAT')));

% Optimization repositories
% L-BFGS-B
addpath(genPathNoGit(fullfile(ROOTDIR, 'optimization/lbfgsb')));
addpath(fullfile(ROOTDIR, 'optimization/box_project'));
% Ipopt
addpath(fullfile(ROOTDIR, ...
    'Ipopt-3.12.7/Ipopt/contrib/MatlabInterface/src'));

% Sparse library repositories
addpath(genPathNoGit(fullfile(ROOTDIR, 'sparse')));

% MATLAB logging tool
addpath(genPathNoGit(fullfile(ROOTDIR, 'logging4matlab')));
end
