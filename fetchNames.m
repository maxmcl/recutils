function [logName, matName] = fetchNames(FACTOR, specType, saveFolder)
%% FetchNames - Function that finds a correct name for the log and the mat file
%   Input:
%   - factor: the factor by which the resolution is decreased compared to
%   the original phantom. Goes in the filename.
%   - specType: string containing reconstruction algorithm information
%   - saveFolder: The folder to look for files with the same name.
%   Outputs:
%   - logName: name for the log of the command window
%   - matName: name for .mat of the run data

% Looking for a name under which to save the run data and the log of the
% command window
counter = 1;
logName = [saveFolder, 'fact_', num2str(FACTOR), '_', specType, '_', ...
    num2str(counter), '.txt'];
while exist(logName, 'file') == 2
    counter = counter + 1;
    logName = [saveFolder, 'fact_', num2str(FACTOR), '_', specType, ...
        '_', num2str(counter), '.txt'];
end
counter = 1;
matName = [saveFolder, 'fact_', num2str(FACTOR), '_', specType, ...
    '_', num2str(counter), '.mat'];
while exist(matName, 'file') == 2
    counter = counter + 1;
    matName = [saveFolder, 'fact_', num2str(FACTOR), '_', specType, ...
        '_', num2str(counter), '.mat'];
end

end
